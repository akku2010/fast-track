import { Component, ChangeDetectorRef } from "@angular/core";
import { ViewController, NavParams, ToastController, AlertController, Events, Platform } from "ionic-angular";
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { TranslateService } from "@ngx-translate/core";
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import { SMS } from "@ionic-native/sms";

@Component({
  selector: 'page-immobilize',
  templateUrl: './immobilize-modal.html'
})
export class ImmobilizeModelPage {
  min_time: any = "10";
  obj: any;
  dataEngine: any;
  DeviceConfigStatus: any;
  immobType: any;
  messages: any;
  checkedPass: string;
  islogin: any;
  respMsg: any;
  intervalID: any;
  commandStatus: any;
  timeoutId: any;
  timeoutSeconds: number = 60000;

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private speechRecognition: SpeechRecognition,
    public translate: TranslateService,
    private toastCtrl: ToastController,
    private apiCall: ApiServiceProvider,
    public alertCtrl: AlertController,
    private events: Events,
    private sms: SMS,
    private plt: Platform,
    private cd: ChangeDetectorRef
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    if (navParams.get("param") != null) {
      this.obj = this.navParams.get("param");
    }
    if (localStorage.getItem("AlreadyDimissed") !== null) {
      localStorage.removeItem("AlreadyDimissed");
    }
  }
  matches: String[];
  isRecording = false;
  isIos() {
    return this.plt.is('ios');
  }

  stopListening() {
    this.speechRecognition.stopListening().then(() => {
      this.isRecording = false;

    });
  }

  getPermission() {
    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => {
        if (!hasPermission) {
          this.speechRecognition.requestPermission();
        }
      });
  }

  startListening() {
    this.getPermission();
    let options = {
      language: 'en-US'
    }
    this.speechRecognition.startListening(options).subscribe(matches => {
      this.matches = matches;
      this.cd.detectChanges();
      console.log("matech length: ", this.matches.length);
      console.log("match found: ", this.matches[0])
      for (var i = 0; i < this.matches.length; i++) {
        if (this.matches[i] === 'Lock engine' || this.matches[i] === 'Unlock Engine' || this.matches[i] === 'lock engine' || this.matches[i] === 'unlock engine') {
          this.IgnitionOnOff(this.obj);
        }
      }
    });
    this.isRecording = true;
  }

  dismiss() {
    if (localStorage.getItem("AlreadyDimissed") !== null) {
      this.events.publish("Released:Dismiss");
      localStorage.removeItem("AlreadyDimissed");
    }
    console.log("Inside the function");
    this.viewCtrl.dismiss();
    localStorage.setItem("AlreadyDimissed", "true");
  }

  voiceToText() {
    // debugger
    // Check feature available
    this.speechRecognition.isRecognitionAvailable()
      .then((available: boolean) => console.log(available));

    // Check permission
    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => {
        console.log(hasPermission)
        if (!hasPermission) {
          // Request permissions
          this.speechRecognition.requestPermission()
            .then(
              () => {
                console.log('Granted');
                this.startListning();
              },
              () => console.log('Denied')
            )
        } else {
          let options = {
            // Android only
            showPartial: true
          }
          this.startListning();
        }
      });
  }

  startListning() {
    // Start the recognition process
    this.speechRecognition.startListening()
      .subscribe(
        (matches: Array<string>) => {
          console.log(matches)
          for (var i = 0; i < matches.length; i++) {
            if (matches[i] === 'Lock Engine' || matches[i] === 'lock engine') {
              this.engineCut('1')
            } else if (matches[i] === 'Unlock Engine' || matches[i] === 'unlock engine') {
              this.engineCut('0')
            }
            // if (matches[i] === 'Lock Engine' || matches[i] === 'Unlock Engine' || matches[i] === 'lock engine' || matches[i] === 'unlock engine') {
            //   this.IgnitionOnOff(this.obj);
            // }
          }
        },
        (onerror) => console.log('error:', onerror)
      );
  }

  unlockEngine() {
    // debugger
    let that = this;
    if (this.obj.last_ACC != null || this.obj.last_ACC != undefined) {

      if (localStorage.getItem('AlreadyClicked') !== null) {
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Process ongoing..'),
          duration: 1800,
          position: 'middle'
        });
        toast.present();
      } else {
        this.checkImmobilizePassword();
        this.messages = undefined;
        this.dataEngine = this.obj;
        var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
        this.apiCall.startLoading().present();
        this.apiCall.ignitionoffCall(baseURLp)
          .subscribe(data => {
            this.apiCall.stopLoading();
            this.DeviceConfigStatus = data;
            this.immobType = data[0].imobliser_type;
            var key;
            console.log('ignition lock key: ', this.dataEngine.ignitionLock)
            if (this.dataEngine.ignitionLock == '1') {
              this.messages = this.translate.instant('Do you want to unlock the engine?')
              key = 1;
            } else {
              if (this.dataEngine.ignitionLock == '0') {
                // this.messages = this.translate.instant('Do you want to lock the engine?')
                this.messages = this.translate.instant('The engine is already unlocked...!')
                key = 0;
              }
            }

            if (key === 1) {
              let alert = this.alertCtrl.create({
                message: this.messages,
                buttons: [{
                  text: this.translate.instant('YES'),
                  handler: () => {
                    debugger
                    if (this.immobType == 0 || this.immobType == undefined) {
                      // that.clicked = true;
                      localStorage.setItem('AlreadyClicked', 'true');
                      var devicedetail = {
                        "_id": this.dataEngine._id,
                        "engine_status": !this.dataEngine.engine_status
                      }
                      // this.apiCall.startLoading().present();
                      this.apiCall.deviceupdateCall(devicedetail)
                        .subscribe(response => {
                          // this.apiCall.stopLoading();
                          // this.editdata = response;
                          const toast = this.toastCtrl.create({
                            message: response.message,
                            duration: 2000,
                            position: 'top'
                          });
                          toast.present();
                          // this.responseMessage = "Edit successfully";
                          // this.getdevices();

                          var msg;
                          if (!this.dataEngine.engine_status) {
                            msg = this.DeviceConfigStatus[0].resume_command;
                          }
                          else {
                            msg = this.DeviceConfigStatus[0].immoblizer_command;
                          }

                          this.sms.send(this.obj.sim_number, msg);
                          const toast1 = this.toastCtrl.create({
                            message: this.translate.instant('SMS sent successfully'),
                            duration: 2000,
                            position: 'bottom'
                          });
                          toast1.present();
                          // that.clicked = false;
                          localStorage.removeItem("AlreadyClicked");
                        },
                          error => {
                            // that.clicked = false;
                            localStorage.removeItem("AlreadyClicked");
                            // this.apiCall.stopLoading();
                            console.log(error);
                          });
                    } else {
                      console.log("Call server code here!!")
                      if (that.checkedPass === 'PASSWORD_SET') {
                        this.askForPassword(this.obj);
                        return;
                      }
                      that.serverLevelOnOff(this.obj);
                    }
                  }
                },
                {
                  text: this.translate.instant('NO')
                }]
              });
              alert.present();
            } else {
              let alert = this.alertCtrl.create({
                message: this.messages,
                buttons: ['Okay']
              });
              alert.present();
            }
          },
            error => {
              this.apiCall.stopLoading();
              console.log("some error: ", error._body.message);
            });
      }
    }
  }

  lockEngine() {
    // debugger
    let that = this;
    if (this.obj.last_ACC != null || this.obj.last_ACC != undefined) {

      if (localStorage.getItem('AlreadyClicked') !== null) {
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Process ongoing..'),
          duration: 1800,
          position: 'middle'
        });
        toast.present();
      } else {
        this.checkImmobilizePassword();
        this.messages = undefined;
        this.dataEngine = this.obj;
        var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
        this.apiCall.startLoading().present();
        this.apiCall.ignitionoffCall(baseURLp)
          .subscribe(data => {
            this.apiCall.stopLoading();
            this.DeviceConfigStatus = data;
            this.immobType = data[0].imobliser_type;
            var key;
            console.log('ignition lock key: ', this.dataEngine.ignitionLock)
            if (this.dataEngine.ignitionLock == '1') {
              // this.messages = this.translate.instant('Do you want to unlock the engine?')
              this.messages = this.translate.instant('The engine is already locked...!')

              key = 0;
            } else {
              if (this.dataEngine.ignitionLock == '0') {
                this.messages = this.translate.instant('Do you want to lock the engine?')
                // this.messages = this.translate.instant('The engine is already unlocked...!')
                key = 1;
              }
            }

            if (key === 1) {
              let alert = this.alertCtrl.create({
                message: this.messages,
                buttons: [{
                  text: 'YES',
                  handler: () => {
                    if (this.immobType == 0 || this.immobType == undefined) {
                      // that.clicked = true;
                      localStorage.setItem('AlreadyClicked', 'true');
                      var devicedetail = {
                        "_id": this.dataEngine._id,
                        "engine_status": !this.dataEngine.engine_status
                      }
                      // this.apiCall.startLoading().present();
                      this.apiCall.deviceupdateCall(devicedetail)
                        .subscribe(response => {
                          // this.apiCall.stopLoading();
                          // this.editdata = response;
                          const toast = this.toastCtrl.create({
                            message: response.message,
                            duration: 2000,
                            position: 'top'
                          });
                          toast.present();
                          // this.responseMessage = "Edit successfully";
                          // this.getdevices();

                          var msg;
                          if (!this.dataEngine.engine_status) {
                            msg = this.DeviceConfigStatus[0].resume_command;
                          }
                          else {
                            msg = this.DeviceConfigStatus[0].immoblizer_command;
                          }

                          this.sms.send(this.obj.sim_number, msg);
                          const toast1 = this.toastCtrl.create({
                            message: this.translate.instant('SMS sent successfully'),
                            duration: 2000,
                            position: 'bottom'
                          });
                          toast1.present();
                          // that.clicked = false;
                          localStorage.removeItem("AlreadyClicked");
                        },
                          error => {
                            // that.clicked = false;
                            localStorage.removeItem("AlreadyClicked");
                            // this.apiCall.stopLoading();
                            console.log(error);
                          });
                    } else {
                      console.log("Call server code here!!")
                      if (that.checkedPass === 'PASSWORD_SET') {
                        this.askForPassword(this.obj);
                        return;
                      }
                      that.serverLevelOnOff(this.obj);
                    }
                  }
                },
                {
                  text: this.translate.instant('NO')
                }]
              });
              alert.present();
            } else {
              let alert = this.alertCtrl.create({
                message: this.messages,
                buttons: ['Okay']
              });
              alert.present();
            }
          },
            error => {
              this.apiCall.stopLoading();
              console.log("some error: ", error._body.message);
            });
      }
    }
  }

  IgnitionOnOff(d) {
    let that = this;
    if (d.last_ACC != null || d.last_ACC != undefined) {

      if (localStorage.getItem('AlreadyClicked') !== null) {
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Process ongoing..'),
          duration: 1800,
          position: 'middle'
        });
        toast.present();
      } else {
        this.checkImmobilizePassword();
        this.messages = undefined;
        this.dataEngine = d;
        var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
        this.apiCall.startLoading().present();
        this.apiCall.ignitionoffCall(baseURLp)
          .subscribe(data => {
            this.apiCall.stopLoading();
            this.DeviceConfigStatus = data;
            this.immobType = data[0].imobliser_type;

            if (this.dataEngine.ignitionLock == '1') {
              this.messages = this.translate.instant('Do you want to unlock the engine?')
            } else {
              if (this.dataEngine.ignitionLock == '0') {
                this.messages = this.translate.instant('Do you want to lock the engine?')
              }
            }
            let alert = this.alertCtrl.create({
              message: this.messages,
              buttons: [{
                text: this.translate.instant('YES'),
                handler: () => {
                  if (this.immobType == 0 || this.immobType == undefined) {
                    // that.clicked = true;
                    localStorage.setItem('AlreadyClicked', 'true');
                    var devicedetail = {
                      "_id": this.dataEngine._id,
                      "engine_status": !this.dataEngine.engine_status
                    }

                    this.apiCall.deviceupdateCall(devicedetail)
                      .subscribe(response => {

                        const toast = this.toastCtrl.create({
                          message: response.message,
                          duration: 2000,
                          position: 'top'
                        });
                        toast.present();

                        var msg;
                        if (!this.dataEngine.engine_status) {
                          msg = this.DeviceConfigStatus[0].resume_command;
                        }
                        else {
                          msg = this.DeviceConfigStatus[0].immoblizer_command;
                        }

                        this.sms.send(d.sim_number, msg);
                        const toast1 = this.toastCtrl.create({
                          message: this.translate.instant('SMS sent successfully'),
                          duration: 2000,
                          position: 'bottom'
                        });
                        toast1.present();
                        // that.clicked = false;
                        localStorage.removeItem("AlreadyClicked");
                      },
                        error => {
                          // that.clicked = false;
                          localStorage.removeItem("AlreadyClicked");
                          // this.apiCall.stopLoading();
                          console.log(error);
                        });
                  } else {
                    console.log("Call server code here!!")
                    if (that.checkedPass === 'PASSWORD_SET') {
                      this.askForPassword(d);
                      return;
                    }
                    that.serverLevelOnOff(d);
                  }
                }
              },
              {
                text: this.translate.instant('NO')
              }]
            });
            alert.present();
          },
            error => {
              this.apiCall.stopLoading();
              console.log("some error: ", error._body.message);
            });
      }
    }
  };

  checkImmobilizePassword() {
    const rurl = this.apiCall.mainUrl + 'users/get_user_setting';
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(rurl, Var)
      .subscribe(data => {
        if (!data.engine_cut_psd) {
          this.checkedPass = 'PASSWORD_NOT_SET';
        } else {
          this.checkedPass = 'PASSWORD_SET';
        }
      })
  }

  askForPassword(d) {
    const prompt = this.alertCtrl.create({
      title: 'Enter Password',
      message: "Enter password for engine cut",
      inputs: [
        {
          name: 'password',
          placeholder: 'Password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Proceed',
          handler: data => {
            console.log('Saved clicked');
            console.log("data: ", data)

            this.verifyPassword(data, d);
          }
        }
      ]
    });
    prompt.present();
  }

  verifyPassword(pass, d) {
    const ryurl = this.apiCall.mainUrl + "users/verify_EngineCut_Password";
    var payLd = {
      "uid": this.islogin._id,
      "psd": pass.password
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(ryurl, payLd)
      .subscribe(resp => {
        this.apiCall.stopLoading();
        console.log(resp);
        if (resp.message === 'password not matched') {
          this.toastmsg(resp.message)
          return;
        }
        // this.serverLevelOnOff(d);
        this.immobilizeDevice(d, this.engineCutStatus);
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  toastmsg(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    }).present();
  }

  toastmsg2(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'middle'
    }).present();
  }

  serverLevelOnOff(d) {
    // let that = this;
    // that.clicked = true;
    localStorage.setItem("AlreadyClicked", "true");
    var data = {
      "imei": d.Device_ID,
      "_id": this.dataEngine._id,
      "engine_status": d.ignitionLock,
      "protocol_type": d.device_model.device_type
    }
    // this.apiCall.startLoading().present();
    this.apiCall.serverLevelonoff(data)
      .subscribe(resp => {
        console.log("ignition on off=> ", resp)
        this.respMsg = resp;

        this.intervalID = setInterval(() => {
          this.apiCall.callResponse(this.respMsg._id)
            .subscribe(data => {
              console.log("interval=> " + data)
              this.commandStatus = data.status;

              if (this.commandStatus == 'SUCCESS') {
                clearTimeout(this.timeoutId);
                clearInterval(this.intervalID);
                // that.clicked = false;
                localStorage.removeItem("AlreadyClicked");
                // this.apiCall.stopLoadingnw();
                const toast1 = this.toastCtrl.create({
                  message: this.translate.instant('process has been completed successfully!'),
                  duration: 2000,
                  position: 'middle'
                });
                toast1.present();
                this.dismiss();
              }
            })
        }, 5000);

        let that = this;
        that.timeoutId = setTimeout(() => {
          debugger
          // if (that.commandStatus !== 'SUCCESS') {
          clearTimeout(that.timeoutId);
          clearInterval(that.intervalID);
          console.log("after removinf interval: ", that.intervalID)
          localStorage.removeItem("AlreadyClicked");
          // }
          // if (that.timeoutId) {
          // clearTimeout(that.timeoutId);
          console.log("after removinf timeout: ", that.timeoutId)

          // }
        }, that.timeoutSeconds);
      },
        err => {
          this.apiCall.stopLoading();
          console.log("error in onoff=>", err);
          localStorage.removeItem("AlreadyClicked");
          // that.clicked = false;
        });
  }

  onInfo() {
    const toast = this.toastCtrl.create({
      message: this.translate.instant("Now you can use voice control to use immobilize. Say 'Lock Engine' to lock vehicle engine and say 'Unlock Engine' to unlock vehicle engine."),
      showCloseButton: true,
      closeButtonText: 'Dismiss'
    });
    toast.present();
  }

  engineCutStatus = '';
  engineCut(key) {
    if (localStorage.getItem('AlreadyClicked') !== null) {
      let toast = this.toastCtrl.create({
        message: this.translate.instant('Process ongoing..'),
        duration: 1800,
        position: 'middle'
      });
      toast.present();
    } else {
      let device = this.obj;
      let msg;
      this.engineCutStatus = device.ignitionLock;
      if (key === this.engineCutStatus) {
        if (key === '0') {
          msg = this.translate.instant('The engine is already unlocked...!')
          this.toastmsg2(msg);
        } else if (key === '1') {
          msg = this.translate.instant('The engine is already locked...!')
          this.toastmsg2(msg);
        }
        return;
      }
      const rurl = this.apiCall.mainUrl + 'users/get_user_setting';
      var Var = { uid: this.islogin._id };
      this.apiCall.urlpasseswithdata(rurl, Var)
        .subscribe(res => {
          var engCutPass = res.engine_cut_psd;
          if (engCutPass == undefined) {
            this.immobilizeDevice(device, this.engineCutStatus);
          } else {
            this.askForPassword(device);
          }
        })
    }
  }


  immobilizeDevice(deviceObj, onOFFcmd) {
    //console.log('deviceObj', deviceObj);
    var deviceCommand = (onOFFcmd == '0') ? "OFF" : "ON";
    var devModelId;

    if (deviceCommand == 'ON') {
      this.messages = this.translate.instant('Do you want to unlock the engine?')
      // key = 1;
    } else {
      if (deviceCommand == 'OFF') {
        // this.messages = this.translate.instant('Do you want to lock the engine?')
        this.messages = this.translate.instant('Do you want to lock the engine?')
        // key = 0;
      }
    }

    let alert = this.alertCtrl.create({
      message: this.messages,
      buttons: [{
        text: this.translate.instant('YES'),
        handler: () => {
          localStorage.setItem('AlreadyClicked', 'true');
          var identifyModelType = typeof (deviceObj.device_model);

          if (identifyModelType == 'string') {
            devModelId = deviceObj.device_model;
          } else {
            devModelId = deviceObj.device_model._id;
          }

          var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?id=' + devModelId;
          this.apiCall.startLoading().present();
          this.apiCall.ignitionoffCall(baseURLp)
            .subscribe(resModel => {
              this.apiCall.stopLoading();
              var deviceType = resModel[0].device_type;
              //console.log('resModel', deviceType);
              var cmdqueObj = {
                "imei": deviceObj.Device_ID,
                "_id": deviceObj._id,
                "engine_status": onOFFcmd,
                "protocol_type": deviceType
              }
              this.triggerCmd(cmdqueObj, deviceCommand)
            })
        }
      },
      {
        text: this.translate.instant('NO')
      }]
    })
    alert.present();
  }
  immobilizeChk: boolean = false;
  immobilizeCount: number = 0;
  callLiveTracking = 0;
  status_cmdq: any;
  intervalTimeOut: any;
  shoError: any;
  showErr: boolean = false;
  unlocked: boolean = false;
  locked: boolean = false;
  triggerCmd(cmdObj, deviceCommand) {
    var that = this;
    this.callLiveTracking = 0;
    that.immobilizeCount = 45;
    that.immobilizeChk = true;
    var timer = setInterval(() => {
      that.immobilizeCount--;
      if (that.immobilizeCount === 0) {
        clearInterval(timer);
        localStorage.removeItem('AlreadyClicked');
      }
    }, 1000);

    this.apiCall.serverLevelonoff(cmdObj)
      .subscribe(res => {
        var cmdQId = res._id;
        //console.log('cmdQId', cmdQId);
        this.intervalTimeOut = setTimeout(function () {
          let message = "Please Try After Sometime";
          localStorage.removeItem('AlreadyClicked');
          that.toastmsg2(message);
          that.shoError = setTimeout(() => {
            that.immobilizeChk = false;
            that.showErr = false;
            clearTimeout(that.shoError)
          }, 5000)
          clearInterval(that.status_cmdq);
          clearTimeout(that.intervalTimeOut);

          clearInterval(timer);
        }, 45000)

        that.status_cmdq = setInterval(function () {
          that.apiCall.callResponse(cmdQId)
            .subscribe(res => {
              that.locked = false;
              that.unlocked = false;
              if (res.status == "SUCCESS") {
                // that.Load = false;
                var cc = ''
                if (deviceCommand === 'ON') {
                  cc = 'UNLOCKED';
                  that.unlocked = true;
                }
                if (deviceCommand === 'OFF') {
                  cc = 'LOCKED';
                  that.locked = true;
                }
                let message = 'Vehicle' + ' ' + cc;

                that.showErr = true;
                that.toastmsg2(message);
                ///////////////////////////
                setTimeout(() => {
                  that.viewCtrl.dismiss();
                }, 2000);
                ///////////////////////////
                that.shoError = setTimeout(() => {
                  that.immobilizeChk = false;
                  that.showErr = false;
                  clearTimeout(that.shoError);
                }, 5000)
                //console.log("Console after Getting success",that.callLiveTracking);

                if (that.callLiveTracking === 0) {
                  that.callLiveTracking = 1;
                }
                localStorage.removeItem('AlreadyClicked');
                clearInterval(timer);
                clearTimeout(that.intervalTimeOut)
                clearInterval(that.status_cmdq);
              }
            }, err => {
              // that.errIcon = true;
              let message = "Please Try After Sometime";
              localStorage.removeItem('AlreadyClicked');
              that.showErr = true;
              that.toastmsg2(message);
              // that.ImmErrMsg = message;
              that.shoError = setTimeout(() => {
                that.immobilizeChk = false;
                that.showErr = false;
                clearTimeout(that.shoError)
              }, 5000)
              let action;
              that.immobilizeChk = false;
              clearInterval(timer);
              // that.snackBar.open(message, action, {
              //   duration: 4000,
              // });
              clearInterval(that.status_cmdq);
              clearTimeout(that.intervalTimeOut)
            })
        }, 3000);
      })
  }
}
